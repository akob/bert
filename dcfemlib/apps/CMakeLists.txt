file (GLOB SOURCE_FILES RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" *.cpp)

set(DCFEMLIB_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../src)
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wno-unused-but-set-variable")

if(WIN32)
    add_definitions(-DMINGW)
endif(WIN32)

foreach(programCPP ${SOURCE_FILES})
    get_filename_component(program ${programCPP} NAME_WE)
    add_executable(${program} ${program}.cpp)
    target_link_libraries(${program} dcfemlib )

    target_include_directories(${program} PRIVATE ${DCFEMLIB_INCLUDE_DIR})

    install(TARGETS ${program} DESTINATION bin)
    add_dependencies(bert1 ${program})
endforeach(programCPP)

set(BERT1_SCRIPTS polyScripts.sh koords2vtk)

foreach(program ${BERT1_SCRIPTS})
    message(${program})
    install(PROGRAMS ${program} DESTINATION bin)
endforeach(program)

install(PROGRAMS coords2vtk DESTINATION bin)
