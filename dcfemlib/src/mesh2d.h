// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef MESH2D__H
#define MESH2D__H MESH2D__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "elements.h"
#include "basemesh.h"

using namespace MyMesh;

#include "facet.h"
#include "polygon.h"

class Domain2D;
namespace MyMesh{

//! A mesh container.
/*! Its only a mesh container with an list of nodes, a list of edges and a list of triangles 
  and serveral function i.e. save, load, meshQualityImprovment. */
class DLLEXPORT Mesh2D : public BaseMesh{
public:
  /*! Creates an empty Mesh with full debug infos (tabbing)*/
  Mesh2D( );
  /*! Constructs a copy of mesh */
  Mesh2D( const Mesh2D & mesh );
  Mesh2D( const string & fname );
  Mesh2D( const Facet & facet );
  /* Descructs the mesh and all included objects */
  virtual ~Mesh2D();
  
  BaseElement * createTriangle( Node & a, Node & b, Node & c, double attribute = 1.0){
    return createTriangle_( a, b, c, this->cellCount(), attribute );
  }
  BaseElement * createTriangle( Node & a, Node & b, Node & c, int id ){
    return createTriangle_( a, b, c, id, 1.0 );
  }
  BaseElement * createTriangle( Node & a, Node & b, Node & c, int id, double attribute ){
    return createTriangle_( a, b, c, id, attribute );
  }
  BaseElement * createTriangle6( const vector < Node * > & pNodeVector, int id, double attribute = 1.0 ){
    return createTriangle6_( pNodeVector, id, attribute );
  }
  BaseElement * createEdge( Node * a, Node * b, int id = -1, int marker = 0 ){
    return createEdge_( *a, *b, id, marker );
  }
  BaseElement * createEdge( Node & a, Node & b, int id = -1, int marker = 0 ){
    return createEdge_( a, b, id, marker );
  }
  BaseElement * createEdge3( Node & a, Node & b, Node & c, int id, int marker = 0 ){
    return createEdge3_( a, b, c, id, marker );
  }

  virtual BaseElement * createCell( Node * a, Node * b, Node * c, double attribute = 1.0 ){
    return createTriangle_( *a, *b, *c, this->cellCount(), attribute );
  }
  virtual BaseElement * createCell( Node & a, Node & b, Node & c, double attribute = 1.0 ){
    return createTriangle_( a, b, c, this->cellCount(), attribute );
  }
  virtual BaseElement * createCell( const vector < Node * > & pNodeVector, double attribute ){
    switch( pNodeVector.size() ){
    case 3:
      return createTriangle_( *pNodeVector[ 0 ], *pNodeVector[ 1 ], *pNodeVector[ 2 ], this->cellCount(), attribute ); break;
    case 6:
      return createTriangle6_( pNodeVector, this->cellCount(), attribute ); break;
    }
    return NULL;
  }
  virtual BaseElement * createCellFromNodeIdxVector( const vector < long > & idx, double attribute );

  virtual BaseElement * createBoundary( Node & a, Node & b, int marker = 0 ){
    return createEdge_( a, b, this->edgeCount(), marker );
  }
  virtual BaseElement * createBoundary( Node * a, Node * b, int marker = 0 ){
    return createEdge_( *a, *b, this->edgeCount(), marker );
  }
  virtual BaseElement * createBoundaryFromNodeIdxVector( const vector < long > & idx, int marker );

  //*! Returns the dimension if the mesh. For this kind of meshs, the function allways returns 2,
  int dim() const { return 2; }

  virtual void showInfos(){ 
    cout << "Nodes: " << pNodeVector_.size() 
	 << "\tTriangles: " << pCellVector_.size() 
	 << "\tEdges: " << pBoundaryVector_.size() << endl;
  }

  virtual void createNeighoursInfos(){ }

  virtual int refine();

//   int boundaryCount() const { return pBoundaryVector_.size(); }
    Edge & edge( int i ) const { return dynamic_cast< Edge & >(*pBoundaryVector_[ i ]); } 
    Edge & edge( int i ){ return *dynamic_cast< Edge * >(pBoundaryVector_[ i ]); } 

    virtual BaseElement * findBoundary( BaseElement & n0, BaseElement & n1 );
//     virtual BaseElement & findBoundary( const BaseElement & n0, const BaseElement & n1 ) const {
//         TO_IMPL;  return BaseElement(); }

  void refineBadTriangle( Triangle & triangle );
  virtual void createH2Mesh( const BaseMesh & mesh );
  virtual void createP2Mesh( const BaseMesh & mesh );
  virtual int createRefined( const BaseMesh & mesh, const vector < int > & cellIdx );

  inline size_t edgeCount() const { return boundaryCount(); }

  virtual void createEdges( const Polygon & line, int marker ){
    for ( size_t i = 0; i < line.size() -1; i ++ ){
      this->createEdge( *line[ i ], *line[ i + 1 ], this->edgeCount(),  marker );
    }
  }
  Polygon findAllNodesBetween( Node * pNodeStart, Node * pNodeEnd, int marker );

  //  void createP2Mesh2D( Mesh2D & mesh );
  Edge * findEdge( Node & n0, Node & n1);

  /*! Improves Mesh Quality (0/1 -- bool) nodeMoving, (0/1 -- bool) edgeSwapping
    (0/1) Funktions (Laplace / mod. Laplace) and the count of Iteratione smoothIteration */
  virtual void improveMeshQuality( bool nodeMoving, bool edgeSwapping, int smoothFunktion, int smoothIteration );

  Edge & splitEdge( Node & node, Edge & edge );
  RealPos norm() const { return this->plane().norm(); }
  Plane plane() const;

  int getZ( const Domain2D & domain );

  //** merge without building any connections 
  void merge( const BaseMesh & subMesh );

protected:

  BaseElement * createEdge_( Node & a, Node & b, int id, int marker );
  BaseElement * createEdge3_( Node & a, Node & b, Node & c, int id, int marker );
  BaseElement * createTriangle_( Node & a, Node & b, Node & c, int id, double attribute );
  BaseElement * createTriangle6_( vector < Node* > pNodeVector, int id, double attribute );

  void refineBadTriangles(double angle);
  virtual void qualityCheck();
  virtual void divideElement( BaseElement & element );
  virtual void divideTriangle( Triangle & triangle );
  virtual void divideIregTriangle( Triangle & triangle );
  virtual void divideQuadrangle( Quadrangle & quadrangle );

  virtual void bisectTriangle( Triangle & triangle );
  virtual void bisectTriangle( Triangle & triangle, Edge & edge, Node & node );

//   int loadNodes(const string & fname);
//   int loadElements( const string & fname );
//   int saveElements( const string & fname );
};

// class QuadrangleMesh : public BaseMesh{
// public:
//   QuadrangleMesh( MyLog::Log & arg = * new MyLog::Log );
//   QuadrangleMesh( const QuadrangleMesh & mesh );
//   virtual ~QuadrangleMesh();

//   virtual BaseElement * createElement( BaseElement & a );
//   BaseElement * createElement( Node & a, Node & b, Node & c, Node & d, int id, float attribute );
//   virtual void refine();

// protected:

//   virtual void divideQuadrangle( Quadrangle & Quadrangle );

//   int loadElements( const string & fname );
//   int saveElements( const string & fname );
// };
} // namespace MyMesh

#endif // TRIANGLEMESH__H

/*
$Log: mesh2d.h,v $
Revision 1.25  2008/12/11 15:14:13  carsten
*** empty log message ***

Revision 1.24  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.23  2007/02/05 16:34:10  carsten
*** empty log message ***

Revision 1.22  2006/11/21 19:26:29  carsten
*** empty log message ***

Revision 1.21  2006/09/22 11:05:28  carsten
*** empty log message ***

Revision 1.20  2006/06/06 09:41:49  carsten
*** empty log message ***

Revision 1.19  2006/05/08 16:25:59  carsten
*** empty log message ***

Revision 1.18  2006/04/17 20:58:03  carsten
*** empty log message ***

Revision 1.17  2005/11/09 13:41:49  carsten
*** empty log message ***

Revision 1.16  2005/10/24 09:52:08  carsten
*** empty log message ***

Revision 1.15  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.14  2005/10/09 18:54:44  carsten
*** empty log message ***

Revision 1.13  2005/09/23 11:24:41  carsten
*** empty log message ***

Revision 1.12  2005/08/22 17:28:09  carsten
*** empty log message ***

Revision 1.11  2005/06/28 16:38:16  carsten
*** empty log message ***

Revision 1.10  2005/05/03 15:55:34  carsten
*** empty log message ***

Revision 1.9  2005/04/11 13:12:35  carsten
*** empty log message ***

Revision 1.8  2005/04/07 11:41:08  carsten
*** empty log message ***

Revision 1.7  2005/03/15 16:08:41  carsten
*** empty log message ***

Revision 1.6  2005/03/10 16:04:38  carsten
*** empty log message ***

Revision 1.5  2005/03/08 16:04:45  carsten
*** empty log message ***

Revision 1.4  2005/02/16 19:36:38  carsten
*** empty log message ***

Revision 1.3  2005/02/09 15:11:43  carsten
*** empty log message ***

Revision 1.2  2005/01/06 17:07:42  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
