// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef DCFEMLIB__H
#define DCFEMLIB__H DCFEMLIB__H


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
namespace DCFEMLib{
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "dcfemlib"
#endif
#ifndef PACKAGE_VERSION
#define PACKAGE_VERSION "1.2.0"
#endif
#ifndef PACKAGE_BUGREPORT
#define PACKAGE_BUGREPORT "carsten@resistivity.de"
#endif
#define PACKAGE_AUTHORS "carsten@resistivity.de, thomas@resistivity.de"
}



#ifdef MINGW
#ifdef BUILDING_DLL
#define DLLEXPORT __declspec(dllexport)
#else /* Not BUILDING_DLL */
#define DLLEXPORT __declspec(dllimport)
#endif /* Not BUILDING_DLL */
#else /* NO MINGW */
#define DLLEXPORT
#endif /* NO MINGW */

#ifdef MINGW
#define PATHSEPARATOR "\\"
namespace DCFEMLib{
//#undefine PACKAGE_NAME
//#define PACKAGE_NAME "dcfemlib-mingw"
}
#else
#define PATHSEPARATOR "/"
#endif


#ifdef MINGW
typedef unsigned int uint;
#endif

//** for sun machines and gcc
#if defined ( __APPLE__ ) || ( defined (__SVR4) && defined (__sun) )
#include <ieeefp.h>
#include <unistd.h>
  int isinf(double x);
#endif

#include <map>
#include <cassert>

#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include <cerrno>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <stdint.h>

#include <sys/timeb.h>
#include <map>
using namespace std;

namespace DCFEMLib{

typedef uint8_t  uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef int32_t  int32;

#ifndef __ASSERT_FUNCTION
#define __ASSERT_FUNCTION "__ASSERT_FUNCTION"
#endif

#define NOT_DEFINED "notDefined"
#define WHERE __FILE__ << "\t"<< __LINE__
#define WHERE_AM_I WHERE << "\t" << __ASSERT_FUNCTION << " "
#define TO_IMPL cerr << WHERE_AM_I << " not yet implemented\n " << versionStr() << "\nPlease send the messages above, the commandline and all necessary data to the author." << endl;
#define TO_CHECK cerr << WHERE_AM_I << " pls check this function." << endl;
#define FUTILE cerr << WHERE_AM_I << " This function is futile. Do not use it." << endl;
#define BASEFUNCT cerr << WHERE_AM_I << " This function is a virtual basefunction. Do not use it." << endl;
#define DO_NOT_USE cerr << WHERE_AM_I << " do not use until further checks !!!!!!." << endl;

#define TOLERANCE 1e-12

#define MESHBINSUFFIX ".bms"

#define HOMOGEN_NEUMANN_BC -1
#define MIXED_BC -2
#define HOMOGEN_DIRICHLET_BC -3
#define DIRICHLET_BC -4

#define ELECTRODE_MARKER -99
#define REFERENCELECTRODE_MARKER -999
#define CALIBRATION_MARKER -1000

enum IOFormat{Ascii,Binary};
enum SpaceConfigEnum{HALFSPACE, FULLSPACE, MIRRORSOURCE, PLANE, PLANE2D};
enum SolverEnum{CG, PCG, ICCG, SSOR, MG, TAUCSWRAPPER, LDLWRAPPER, CHOLMODWRAPPER, SOLVERTEST, ANALYT};

DLLEXPORT string versionStr();
DLLEXPORT int openInFile( const string & fname, fstream * file, bool verbose = true );
DLLEXPORT int openOutFile( const string & fname, fstream * file, bool verbose = true );

DLLEXPORT int countColumns( const string & fname );
DLLEXPORT int countColumns( fstream & file );
DLLEXPORT vector < string > getRowSubstrings( fstream & file, char comment = '#' );
DLLEXPORT vector < string > getNonEmptyRow( fstream & file, char comment = '#' );
DLLEXPORT vector < string > getSubstrings( const string & sumString );
DLLEXPORT map < float, float > loadFloatMap( const string & filename );
DLLEXPORT int findDomainDimension( const string & filename );

inline int toInt( const string & str ){ return atoi( str.c_str() ); }
inline float toFloat( const string & str ){ return strtof( str.c_str(), NULL ); }
inline double toDouble( const string & str ){ return strtod( str.c_str(), NULL ); }

//** for testsuites, compare to values, for an example see: tests/vectors/operators.cpp
template < class T > bool compare( T expect , T found, bool verbose ){
  if ( verbose && expect == found ) cout << "... ok, ";
  else if ( verbose ) {
    cout << expect << " != " << found <<  " test ...fail, ";
  }
  return expect == found;
}

//** Generell template for conversion to string, shoul supersede all sprintf etc.
template< class T > inline std::string toStr( const T & value ){
  std::ostringstream streamOut;
  streamOut << value;
  return streamOut.str();
}

inline bool fileExist( const std::string & filename ){
  bool result = false;
  std::ifstream file; file.open( filename.c_str() );
  if ( file ) {
    result = true;
    file.close();
  }
  return result;
}

bool compare( double expect, double found, double tol, bool verbose );

class Stopwatch {
public:
  Stopwatch() { state = undefined; }
  void start(){
    ftime( & starttime );
    state = running;
  }
  void stop( bool verbose = false ){
    ftime( & stoptime );
    state = halted;
    if ( verbose ) cout << "time: " << duration() << "s" << endl;
  }
  /*! Restart the stopwatch.*/
  void restart(){
    stop();
    start();
  }
  /*! Reset the stopwatch, same like \ref restart.*/
  void reset(){ restart(); }

  double duration(){
    if ( state == undefined ) cerr << "Stopwatch not started!" << endl;;
    if ( state == running ) ftime( &stoptime );
    return double( stoptime.time - starttime.time )
      + double( stoptime.millitm - starttime.millitm ) / 1000.0;
  }
protected:
  timeb starttime, stoptime;
  enum watchstate {undefined,halted,running} state;
};

} // namespace DCFEMLib{

#endif //DCFEMLIB__H

/*
$Log: dcfemlib.h,v $
Revision 1.56  2011/01/24 11:13:05  thomas
added -O option into polyCreateCube
changed some example files (3dtank and acucar)

Revision 1.55  2010/10/28 12:01:28  carsten
fix version conflict

Revision 1.54  2010/06/14 11:53:55  carsten
*** empty log message ***

Revision 1.53  2010/02/05 17:22:00  carsten
transfer

Revision 1.52  2010/01/31 10:49:54  carsten
*** empty log message ***

Revision 1.51  2008/05/19 14:52:28  carsten
Add cholmodwrapper, interpolate V.2 in createSurface

Revision 1.50  2007/12/04 20:10:50  carsten
*** empty log message ***

Revision 1.49  2007/10/04 07:21:59  thomas
BERT + polytools codeblocks (dllexport)

Revision 1.48  2007/07/31 18:05:35  thomas
no message

Revision 1.47  2007/03/03 15:35:41  carsten
*** empty log message ***

Revision 1.46  2006/11/21 19:26:29  carsten
*** empty log message ***

Revision 1.45  2006/08/02 18:51:13  carsten
*** empty log message ***

Revision 1.44  2006/08/02 18:24:19  carsten
*** empty log message ***

Revision 1.43  2006/07/24 16:24:14  carsten
*** empty log message ***

Revision 1.42  2006/07/19 19:20:18  carsten
*** empty log message ***

Revision 1.41  2006/04/05 19:30:07  carsten
*** empty log message ***

Revision 1.40  2006/04/05 19:00:13  carsten
*** empty log message ***

Revision 1.39  2006/03/30 15:35:48  carsten
*** empty log message ***

Revision 1.38  2006/02/14 11:09:18  carsten
*** empty log message ***

Revision 1.37  2006/01/22 21:02:06  carsten
*** empty log message ***

Revision 1.36  2005/12/06 13:57:26  carsten
*** empty log message ***


Revision 1.33  2005/10/12 14:41:56  carsten
*** empty log message ***

Revision 1.32  2005/10/12 12:35:21  thomas
*** empty log message ***

Revision 1.31  2005/09/08 19:08:48  carsten
*** empty log message ***

Revision 1.30  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.29  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.28  2005/06/23 20:15:32  carsten
*** empty log message ***

Revision 1.27  2005/05/31 10:50:04  carsten
*** empty log message ***

Revision 1.26  2005/03/30 11:56:53  carsten
*** empty log message ***

Revision 1.25  2005/03/15 18:12:01  carsten
*** empty log message ***

Revision 1.24  2005/03/15 18:07:19  carsten
*** empty log message ***

Revision 1.23  2005/03/15 17:51:19  carsten
*** empty log message ***

Revision 1.22  2005/03/15 17:46:02  carsten
*** empty log message ***

Revision 1.21  2005/03/15 17:36:52  carsten
*** empty log message ***

Revision 1.20  2005/03/15 17:32:17  carsten
*** empty log message ***

Revision 1.19  2005/03/15 17:30:47  carsten
*** empty log message ***

Revision 1.18  2005/02/14 17:40:08  carsten
*** empty log message ***

Revision 1.17  2005/02/11 17:12:24  carsten
Friday commit

Revision 1.16  2005/02/10 14:07:59  carsten
*** empty log message ***

Revision 1.15  2005/02/09 15:11:43  carsten
*** empty log message ***

Revision 1.14  2005/02/07 13:33:13  carsten
*** empty log message ***

Revision 1.13  2005/01/17 13:44:48  carsten
*** empty log message ***

Revision 1.12  2005/01/14 12:51:27  carsten
*** empty log message ***

Revision 1.11  2005/01/13 21:12:15  carsten
*** empty log message ***

Revision 1.10  2005/01/13 16:04:09  carsten
*** empty log message ***

Revision 1.9  2005/01/13 16:03:01  carsten
*** empty log message ***

Revision 1.8  2005/01/11 19:12:26  carsten
*** empty log message ***

Revision 1.7  2005/01/06 20:28:30  carsten
*** empty log message ***

Revision 1.6  2005/01/06 14:16:14  carsten
*** empty log message ***

Revision 1.5  2005/01/06 13:20:07  carsten
*** empty log message ***

Revision 1.4  2005/01/06 11:13:09  carsten
*** empty log message ***

Revision 1.3  2005/01/05 15:01:11  carsten
*** empty log message ***

Revision 1.2  2005/01/04 19:22:30  carsten
add in cvs

Revision 1.1.1.1  2005/01/03 18:00:09  carsten

start cvs dcfemlib

*/
