// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef MATRIX__H
#define MATRIX__H MATRIX__H

#include "vector.h"

// #include <iostream>
// #include <cstdio>
// #include <string>
// #include <valarray>
// #include <cassert>
#include <map>

using namespace std;

namespace MyVec{

template <class T> class BasisMatrix;
template <class T> class Matrix;
template <class T> class DirectMatrix;

}

typedef MyVec::DirectMatrix< real_ > RDirectMatrix; 
typedef MyVec::DirectMatrix< double > DSparseMatrix;
namespace MyVec{

//!
/*!*/
template <class T> class BasisMatrix{

public:
  BasisMatrix(){}                                    //** Standartkonstruktor
  virtual ~BasisMatrix(){}                           //** Standartdestruktor

  BasisMatrix(int){}
  BasisMatrix(const BasisMatrix<T> &){} 
  virtual void clear(){}
  void show() {
    cout << size_ << endl;
    cout << endl;
    for (int i=1; i <= size_; i++){
      for (int j=1; j <= size_; j++) cout << GetVal(i, j) << "\t";
      cout << endl;
    }
    cout << endl;
  }
  void show() const {
    cout << size_ << endl;
    cout << endl;
    for (int i=1; i<=size_; i++){
      for (int j=1; j<=size_; j++) cout << GetVal(i, j) << "\t";
      cout << endl;
    }
    cout << endl;
  }
  virtual int size() { return size_; }
  virtual int size() const { return size_; }

  virtual int rows() { return size_; }
  virtual int rows() const { return size_; }

  virtual void SetVal(int, int, T){}
  virtual void AddVal(int, int, T){}
  virtual void SubVal(int, int, T){}
  virtual void MulVal(int, int, T){}
  virtual void DiffVal(int, int, T){}
  virtual T GetVal(int, int) const {return(0);}
  virtual T GetVal(int, int) {return(0);}
  
  virtual BasisMatrix<T> & operator=(const BasisMatrix<T> &){ return(*this); }
  // !!! BasisMatrix Operatoren noch nicht gaenzlich klar bei Funktionsaufruf !!!
  // zu versuchen die Operatoren zu implementieren und die Basisklassen 
  // auf die Childklasse zu casten :)


  //virtual BasisMatrix<T> & operator=(const ElementMatrix & A) {return (*this); }
  //virtual BasisMatrix<T> & operator+=(const BasisMatrix<T> & A) {return (*this); }
  
protected:
  int size_;
};


template<class T> class DirectMatrix{
//template<class T> class DirectMatrix : public BasisMatrix<T>{
public:
  /*! Konstructor */
  DirectMatrix( int n = 0 ){
    gamma_ = 0;
    size_ = n;
    initMem();
  }
  /*! Destructor  */
  ~DirectMatrix(){ 
    freeMem(); 
  }
  DirectMatrix( const DirectMatrix<T> & A ); /*      Kopierkonstruktor      */

  DirectMatrix< T > & operator = ( const DirectMatrix< T > & A );

  template< class Matrix > DirectMatrix<T> & operator += ( const Matrix & A ){
    int dumm = 1;
    
    assert( (int)size_ >= A.size() );

    for ( int i = 0, imax = A.size(); i < imax; i++ ){
      for ( int j = 0, jmax = A.size(); j < jmax; j++ ){
	//     	cout << i << "\t" << j << "\t" 
	//    	     << A.i(i) << "\t" << A.i(j) << "\t" << A.at(i,j) << endl; 
	AddVal( (int)A.i( i ) + dumm, (int)A.i( j ) + dumm, A.at( i, j ) );
      }
    }
    return *this;
  }

  void freeMem(){
    for ( int i = 0; i < size_; i ++ ) delete[] mat[ i ];
    delete[] mat;
  }
  void initMem(){
    mat = new T * [ size_ ]; assert(mat);
    for ( int i = 0; i < size_; i++ ){
      mat[ i ] = new T[ 1 ]; assert( mat[ i ] );
      mat[ i ][ 0 ] = 0;
    }
  }
  int valueCount() const {
    int nVals = 0;
    for ( int i = 0; i < size_; i ++ ){
      nVals += (int)mat[ i ][ 0 ];
    }
    return nVals;
  }

  /*! set size = 1 */
  void clear();
  /*! clean all vals, size conatains */
  void clean();
  void show();
  void showFull();

  void SetVal( int i, int j, T x );
  void setVal( int i, int j, T x ){ SetVal( i + 1, j + 1, x ); }

  void AddVal( int i, int j, T x );
  void SubVal( int i, int j, T x );
  void MulVal( int i, int j, T x );
  void DivVal( int i, int j, T x );

  T GetVal( int i, int j ) const;
  T GetVal( int i, int j );

  T val( int i, int j ) const { return GetVal( i + 1, j + 1 ); }
  T val( int i, int j ) { return GetVal( i + 1, j + 1 ); }

  int Size() const { return size_; }
  int size() { return size_; }
  int size() const { return size_; }

  int Gamma() const { return gamma_; }

  void clearRow( int i );
  void clearColumn( int i );
  double multiplicateRow( int row, double fak );
  RVector extractRow( int row );
  map< int, T > extractSparseRow( int row ) const;

  int scale( );
  void createIncompleteCholesky( );
  RVector backwardInsertion( RVector & b );
  RVector transposeForwardInsertion( RVector & b );

  void resize( int newSize ){
    if ( newSize != size_ ){
      DirectMatrix< T > tmp( *this );
      freeMem();
      mat = new T * [ newSize ]; assert(mat);

      int rowSize = 0;
      for ( int i = 0; i < newSize; i++ ){
	if ( i < size_ ){
	  rowSize = (int)tmp.mat[ i ][ 0 ] * 2 + 1;
	  mat[ i ] = new T[ rowSize ]; assert( mat[ i ] );
	  memset( mat[ i ], '\0',  rowSize * sizeof( T ) );
	  memcpy( mat[ i ], tmp.mat[ i ], rowSize * sizeof( T ) );
	} else {
	  mat[ i ] = new T[ 1 ]; assert( mat[ i ] );
	  mat[ i ][ 0 ] = 0;
	}
      }
      size_ = newSize;
    }
  }
   
  int save( const string & fname ) const;
  int load( const string & fname, bool transpose = false );

  template< class Matrix > void copyTo( Matrix & A){
    int column = 0;	
    for( int row = 0; row < size_; row++ ){
      for( int j = 0; j < mat[ row ][ 0 ]; j++ ){
    	column = (int)mat[ row ][ ( j * 2 ) + 1] - 1;
	//A.setval( mat[ row ][ ( j + 1 ) * 2 ], row, column );

	A( row, column ) =  mat[ row ][ ( j + 1 ) * 2 ];

	//A[ row ][ column ] =  mat[ row ][ ( j + 1 ) * 2 ];
      }
    }
  }
  template< class Matrix > void copyToClean( Matrix & A){
//     cerr << WHERE_AM_I << " hier passieren komische Dinge" << endl;
//     exit(0);
    A.clear();
    int column = 0;	
    for( int row = 0; row < size_; row++ ){
      for( int j = 0; j < mat[ row ][ 0 ]; j++ ){
     	column = (int)mat[ row ][ ( j * 2 ) + 1] - 1;
	//A.setval( mat[ row ][ ( j + 1 ) * 2 ], row, column );
	//	  A( row, column ) =  mat[ row ][ ( j + 1 ) * 2 ];
	A[ row ][ column ] =  mat[ row ][ ( j + 1 ) * 2 ];
      }
    }
  }
//   DirectMatrix<T> & operator = (const ElementMatrix & A){
//     cerr << WHERE_AM_I << " Warning! function not permitted." << endl;
// //     int dumm = 1;
// //     assert(size >= A.size());
// //     for (int i=1; i<=A.size(); i++){
// //       for (int j=1; j<=A.size(); j++){
// // //    	cout << i << "\t" << j << "\t" 
// // //    	     << A.GetI(i) << "\t" << A.GetI(j) << "\t" << A.GetVal(i,j) << endl; 
// // 	SetVal(A.i(i) + dumm, A.i(j) + dumm, A.at(i,j));
// //       }
// //     }
//     return *this;
//   }
  DirectMatrix< T > & triLowerLeft( const DirectMatrix<T> & A ) {
//     freeMem();
//     size_ = A.size();
//     initMem();
//     int valueCount = 0, col = 0;
//     for ( int row = 0; row < size_; row ++ ){



//       valueCount = (int)mat[ row ][ 0 ];
//       for ( int m = 0; m < valueCount; m ++ ){
// 	col = (int)mat[ row ][ 1 + 2 * m ];
// 	if ( col <=row ) setVal( row, col, mat[ row ][ 1 + 2 * m + 1] );
//       }
//     }
    TO_IMPL
    return *this;
  }

  template < class Vector > double multLoTri( int row, Vector & vec ) const {
    double t = 0.0;
    int col = 0;
    for ( int m = 1; m <= (int)mat[ row ][ 0 ]; m ++ ){
      if ( ( col = (int)mat[ row ][ 2 * m - 1 ] ) > row ){
	t += mat[ row ][ 2 * m ] * vec[ col ];
      }
    }
    return t;
  }
  
  T **mat;
protected:
  int gamma_;
  int size_;
};

template < class T > DirectMatrix< T > operator * ( DirectMatrix< T > & A, DirectMatrix< T > & B ){
  int dim = A.size();
  DirectMatrix< T > tmp( dim );
  T val = 0.0;
  //  RVector colI, rowJ;
  map< int, T > colI, rowJ;

  for ( int i = 0; i < dim; i ++ ){
    cout << "\r " << i;
    //    colI = A.extractRow( i + 1);
    colI = A.extractSparseRow( i );
    for ( int j = 0; j < dim; j ++ ){
      rowJ = B.extractSparseRow( j );
      //      rowJ = B.extractRow( j + 1 );
      //      val = dot( colI, rowJ );
      val = 0.0;
      for ( map< int, double >::iterator it = colI.begin(); it != colI.end(); it ++){
	if ( fabs(rowJ[ (it->first) ]) < 1e12 ) val += rowJ[ (it->first) ] * (it->second);
      }
  
      if ( fabs(val) < 1e-12 ){
	tmp.SetVal( i + 1, j + 1, val );
      }
    }
  }
  return tmp;
}

template <class T> Vec<T> operator*(const DirectMatrix<T> & A, const Vec<T> & b);

/* Kopierkonstructor */
template < class T > DirectMatrix< T >::DirectMatrix( const DirectMatrix< T > & A ){
  size_ = A.size();
  gamma_ = 0;
  mat = new T * [ size_ ]; assert( mat );

  for( int i = 0; i < size_; i++ ){
    mat[ i ] = new T[ (int)A.mat[ i ][ 0 ] * 2 + 1 ]; assert( mat[ i ] );
  }
  for( int i = 0; i < size_; i++ ){
    memcpy( mat[ i ], A.mat[ i ], ( 1 + (int)A.mat[ i ][ 0 ] * 2 ) * sizeof( T ) );
  }
}

template < class T > DirectMatrix< T > & DirectMatrix< T >::operator = ( const DirectMatrix< T > & A ){
  if ( this != &A ){
    freeMem();
    size_ = A.size();
    mat = new T * [ size_ ]; assert( mat );
    gamma_ = 0;
    int valCount = 0;
    
    for( int i = 0; i < size_; i++ ){
      valCount = (int)A.mat[ i ][ 0 ];
      mat[ i ] = new T[ valCount * 2 + 1 ]; assert( mat[ i ] );
      memcpy( mat[ i ], A.mat[ i ], ( valCount * 2 + 1 ) * sizeof( T ) );
    }
  } return *this;
}

template < class T > void DirectMatrix< T >::createIncompleteCholesky( ){
  
  int nrOfVals = 0, col = 0;
  T diagSquare = 0.0;
  double dummy = 0.0;
    
  for ( int row = 0; row < size_; row ++){
    cout << "\r" << row << "/" << size_;
//     setVal( row, row, sqrt( val( row, row ) ) );
//     cout << row  <<"\t" << val( row, row ) <<"\t" <<sqrt( val( row, row ) ) << endl;

    nrOfVals = (int)mat[ row ][ 0 ];
    for ( int h = 1; h < nrOfVals * 2; h += 2 ){
      col = (int)mat[ row ][ h ] - 1;
      if ( col == row ){
 	diagSquare = sqrt( mat[ row ][ h + 1 ] );
	mat[ row ][ h + 1 ] = diagSquare;
	break;
      } 
    }
    for ( int h = 1; h < nrOfVals * 2; h += 2 ){
      col = (int)mat[ row ][ h ] - 1;
      if ( col > row ) mat[ row ][ h + 1 ] /= diagSquare;
    }
    
    for ( int j = row + 1; j < size_; j ++ ){
      nrOfVals = (int)mat[ j ][ 0 ];
      dummy = val( row, j );
      for ( int h = 1; h < nrOfVals * 2; h += 2 ){
	col = (int)mat[ j ][ h ] - 1;
 	if ( col >= j ){
	  setVal( j, col, val( j, col ) - val( row, col ) * dummy );
 	}
      }
    }
  }
  cout << endl;
}

template <class T> RVector DirectMatrix<T>::transposeForwardInsertion( RVector & b ){
  RVector result( size_ );
  T tmp = 0.0;//, diag = 0.0;
  //  int nrOfVals = 0, col = 0;
  
  for ( int row = 0; row < size_; row ++ ){
    tmp = b[ row ];
    for ( int col = 0; (int)col < ((int)row); col ++ ){
      if ( fabs( val( col, row ) ) > 1e-40 ){
	tmp -= val( col, row ) * result[ col ];
      }
    }
    result[ row ] = tmp / val( row, row );
  }
  return result;
}

template <class T> RVector DirectMatrix<T>::backwardInsertion( RVector & b ){
  RVector result( size_ );
  T tmp = 0.0, diag = 0.0;
  int nrOfVals = 0, col = 0;
  for ( int row = size_ - 1; (int)row > -1; row -- ){
    tmp = b[ row ];
    
    nrOfVals = (int)mat[ row ][ 0 ];
    for ( int h = 1; h < nrOfVals * 2; h += 2 ){
      col = (int)mat[ row ][ h ] - 1;
      if ( col == row ) {
	diag = mat[ row ][ h + 1];
      } else if ( col > row ){
	tmp += mat[ row ][ h + 1] * result[col];
      }
    }
    result[ row ] = -1 * tmp / diag;
  }
  return result;
}


template <class T> void DirectMatrix<T>::clear(){
  freeMem();
  size_ = 1;
  initMem();
}

template <class T> void DirectMatrix<T>::clean(){
  freeMem();
  initMem();
}

template <class T> void DirectMatrix<T>::show(){

  map < int, double > row;
  for( int i = 0; i < size_; i++ ){
    for( int j = 0; j < mat[ i ][ 0 ]; j++ ){
      row[ (int)mat[ i ][ ( j * 2 ) + 1] ] = mat[ i ][ ( j + 1 ) * 2 ];
    }
    for ( map< int, double>::iterator it = row.begin(); it != row.end(); it ++){
      cout << "(" << it->first << "," << i + 1 << ")\t"<< it->second << endl;
    }
    row.clear();
  }
}

template <class T> void DirectMatrix<T>::showFull(){
  cout << endl;
  for ( int i = 1; i <= size_; i++){
    for ( int j = 1; j <= size_; j++ ){
      cout << GetVal(i, j) << " ";
    }
    cout << endl;
  }
  cout << endl;
}

template <class T> void DirectMatrix<T>::clearRow( int row ){
  assert( row > 0 && row <= (int)size_ );

  for ( int i = 0; i < size_; i ++ ){
    SetVal( row, i + 1, 0.0 );
  }
}

template <class T> void DirectMatrix<T>::clearColumn( int col ){
  assert( col > 0 && col <= (int)size_ );
  
  for ( int i = 0; i < size_; i ++ ){
    SetVal( i + 1, col, 0.0 );
  }
}

template <class T> map< int, T > DirectMatrix<T>::extractSparseRow( int row ) const {
  map< int, T > result;
  
  assert( row > -1 && row < (int)size_ );
  int nVals = (int)mat[ row ][ 0 ];

  for ( int h = 1; h < nVals * 2; h += 2 ){
    result[ (int)mat[ row ][ h ] - 1 ] =  mat[ row ][ h + 1 ];
  }
  return result;
}

template <class T> RVector DirectMatrix<T>::extractRow( int row ){
  RVector result( size_ );

  assert( row > 0 && row <= size_ );
  int nVals = (int)mat[ row - 1 ][ 0 ];

  for ( int h = 1; h < nVals * 2; h += 2 ){
    result[ (int)mat[ row - 1 ][ h ] - 1 ] =  mat[ row - 1 ][ h + 1 ];
  }
  return result;
}

template <class T> double DirectMatrix<T>::multiplicateRow( int row, double fak ){
  double result = 0.0;
  
  assert( row > 0 && row <= size_ );
  int nVals = (int)mat[ row - 1 ][ 0 ];

  for ( int h = 1; h < nVals * 2; h += 2 ){
    result += fak * mat[ row - 1 ][ h + 1 ];
  }
  return result;
}

template <class T> T DirectMatrix<T>::GetVal(int i, int j)const{
  //  if ( j > i ) return GetVal( j, i );
  assert( i > 0 && i <= size_ );

  int numberOfVals = (int)mat[ i - 1 ][ 0 ];
  if ( j == 0 ) return numberOfVals;

  for ( int h = 1; h < numberOfVals * 2; h += 2 ){
    if ( (int)mat[ i - 1][ h ] == j ) return mat[ i - 1 ][ h + 1 ];
  }
  return 0.0;
}

template <class T> T DirectMatrix<T>::GetVal(int i, int j){
  //  if ( j > i ) return GetVal( j, i );
  assert( i > 0 && i <= size_ );

  int numberOfVals = (int)mat[ i - 1][ 0 ];
  if ( j == 0 ) return numberOfVals;

  for ( int h = 1; h < numberOfVals * 2; h += 2 ){
    if ( (int)mat[ i - 1][ h ] == j ) return mat[ i - 1 ][ h + 1 ];
  }
  return 0.0;
}

template <class T> void DirectMatrix<T>::SetVal( int i, int j, T x ){
  /* [Anzahl der Elemente(n), n1, x1, n2, x2, n3, x3, ..., nn, xn]*/
  //  if ( j > i ) SetVal( j, i, x);
  
  //  cout << i << " " << size_ << endl;
  assert( i > 0 && i <= size_ );
  

  int n = (int)mat[ i - 1 ][ 0 ], h = 0;
  
  if ( n > gamma_ ) gamma_ = n; /* max anzahl von Elementen pro Zeile der gesam. Matrix */

  for ( h = 1; h <= n * 2; h += 2 ){
    if ( (int)mat[ i - 1][ h ] == j){
      mat[ i -1 ][ h + 1] = x; 
      return;
    }
  }

  if ( fabs( x ) < 1E-40 ) {
//     //    cout << WHERE_AM_I << " Warning!! " << i -1 << " " << j -1 << " " << x << endl;
    return; //wenn ich expliziet auf Null sezte wird der Wert nicht gesetzt
  }

  T *tmp = new T[ n * 2 + 1 ];
  memset(tmp, '0', (2*n+1)*sizeof(T));
  memcpy(tmp, mat[i-1], (2*n+1)*sizeof(T));
  
  delete [ ] mat[ i - 1 ];
  mat[ i - 1 ] = new T[ ( n * 2 ) + 3 ];
  
  memcpy(mat[i-1], tmp, (2*n+1)*sizeof(T));
  
  mat[i-1][0]++;
  mat[i-1][(n*2)+1]=j;
  mat[i-1][(n*2)+2]=x;
  delete [] tmp;
  return;
}

template <class T> void DirectMatrix<T>::AddVal( int i, int j, T x ){
  //  if ( j > i ) AddVal( j, i, x);

  if ( fabs( x ) < 1E-40 ) return;

  //  cout << i << "\t" << size_ << endl;
  assert( i > 0 && i <= size_ );
  int n = (int)mat[ i - 1 ][ 0 ];
  
//   if (n > gamma_) {
//     gamma = n; 
//     //    printf("\n%ld", gamma);
//   }

  for ( int h = 1; h <= n * 2; h += 2 ){
    if ( (int)mat[ i - 1 ][ h ] == j ){
      mat[ i - 1 ][ h + 1 ] += x;
      return;
    }
  }
  
  T *tmp = new T[n*2+1];
  memset(tmp, '0', (2*n+1)*sizeof(T));
  memcpy(tmp, mat[i-1], (2*n+1)*sizeof(T));
  
  delete[] mat[i-1];
  mat[i-1] = new T[(n*2)+3];
  
  memcpy(mat[i-1], tmp, (2*n+1)*sizeof(T));
  
  mat[i-1][0]++;
  mat[i-1][(n*2)+1]=j;
  mat[i-1][(n*2)+2]=x;
  delete[] tmp;
  return;
}

template <class T> void DirectMatrix<T>::SubVal(int i, int j, T x){
  cout << __FILE__<< " at line "
       << __LINE__<< "Noch nicht implementiert.\n";
}

template <class T> void DirectMatrix<T>::MulVal(int i, int j, T x){
  cout << __FILE__<< " at line "
       << __LINE__<< "Noch nicht implementiert.\n";
}

template <class T> void DirectMatrix<T>::DivVal(int i, int j, T x){
  cout << __FILE__<< " at line "
       << __LINE__<< "Noch nicht implementiert.\n";
}


template <class T> Vec<T> operator*( const DirectMatrix<T> & A, const Vec<T> & b ){
  assert( A.size() == b.size() );
  Vec<T> tmp( b.size() );
  int size_ = b.size();
  int j;
  T t = 0.;
  //  printf("Adresse S =%x\t%x\n",&A, &b);
  for (int i=1; i<=size_; i++){
    t=0;
    for (int m=1; m<=(int)A.mat[i-1][0]; m++){
      j=(int)A.mat[i-1][2*m-1];
      t+=A.mat[i-1][2*m] * b.array_[ j - 1 ];
    }
    tmp[i-1] = t;
  }
  return(tmp);
}

template <class T> int DirectMatrix<T>::load( const string & fname, bool transpose ){
  fstream file; openInFile( fname, & file );
  int i = 0, j = 0;
  int count = 0;
  bool diagMatrix = false;
  double val = 0.0;
    while( file >> i >> j >> val){
      count ++;
      //      cout << i << " " << j << endl;
      if ( diagMatrix && j > i ) continue;
      
      if ( transpose ) setVal( j, i, val);
      else setVal( i, j, val);
      
    }
    if ( j+1 < size() ) cerr << WHERE_AM_I << "Warning!! To few datas. " << endl;
    if ( count != valueCount() ) cerr << WHERE_AM_I << "Warning!! count != valueCount. " 
				      << count << " != "<< valueCount()<< endl;
  file.close();
  return 1;
}

template <class T> int DirectMatrix<T>::save( const string & fname ) const {
  fstream file; openOutFile( fname, & file );

  map < int, double > tmp;
  file.setf( ios::scientific, ios::floatfield );
  file.precision( 14 );
  for( int row = 0; row < size_; row++ ){
    tmp = extractSparseRow( row );
    for ( map< int, double>::iterator it = tmp.begin(); it != tmp.end(); it ++){
      file << row << "\t" << it->first << "\t" << it->second << endl;
    }
  }
  file.close();
  return 1;
}

template <class T> int DirectMatrix<T>::scale( ){
  RVector d( size_ );
  for (int i = 0, imax = size_; i < imax ; i++){
    d[ i ] = 1 / sqrt( GetVal( i + 1, i + 1 ) );
  }

  for( int i = 0; i < size_; i++ ){
    for( int j = 0; j < mat[ i ][ 0 ]; j++ ){
//       cout << i << "\t" <<  mat[ i ][ ( j * 2 ) + 1 ] - 1 << "\t" << mat[ i ][ ( j * 2 ) + 2 ] << "\t" 
// 	   << d[ mat[ i ][ ( j * 2 ) + 1] - 1 ] << "\t" << d[ i ] << "\t"
// 	   << mat[ i ][ ( j * 2 ) + 2 ]* d[ mat[ i ][ ( j * 2 ) + 1] - 1 ] * d[ i ] << endl;
      mat[ i ][ ( j * 2 ) + 2 ] = mat[ i ][ ( j * 2 ) + 2 ] * d[ mat[ i ][ ( j * 2 ) + 1] - 1 ] * d[ i ];
      
    }
  }
  return 1;
}
}
#endif

/*
$Log: matrix.h,v $
Revision 1.15  2007/04/19 18:42:54  carsten
*** empty log message ***

Revision 1.14  2007/03/07 16:38:14  carsten
*** empty log message ***

Revision 1.13  2006/11/21 19:26:29  carsten
*** empty log message ***

Revision 1.12  2005/10/17 15:10:17  carsten
*** empty log message ***

Revision 1.11  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.10  2005/10/14 15:31:37  carsten
*** empty log message ***

Revision 1.9  2005/10/12 14:41:56  carsten
*** empty log message ***

Revision 1.8  2005/09/30 18:09:40  carsten
*** empty log message ***

Revision 1.7  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.6  2005/06/02 15:44:10  carsten
*** empty log message ***

Revision 1.5  2005/05/31 10:50:04  carsten
*** empty log message ***

Revision 1.4  2005/01/13 11:58:48  carsten
*** empty log message ***

Revision 1.3  2005/01/11 19:12:26  carsten
*** empty log message ***

Revision 1.2  2005/01/07 18:02:30  carsten
*** empty log message ***

Revision 1.1  2005/01/06 20:28:30  carsten
*** empty log message ***


*/
