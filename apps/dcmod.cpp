/***************************************************************************
 *   Copyright (C) 2006-2019 by the resistivity.net development team       *
 *   Carsten Rücker carsten@resistivity.net                                *
 *   Thomas Günther thomas@resistivity.net                                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <bert.h>

#include <interpolate.h>
#include <optionmap.h>

using namespace GIMLI;

#include <memwatch.h>

#include <string>

int main(int argc, char * argv[]) {
    setbuf(stdout, NULL);

    //** define default values;
    std::string attributemap = NOT_DEFINED, boundarymap = NOT_DEFINED, bypassmap  = NOT_DEFINED;
    std::string schemeFile = NOT_DEFINED, potentialsFileName = NOT_DEFINED, primPotFileBody = NOT_DEFINED;
    std::string meshToInterpolate   = NOT_DEFINED, outputFileBody = "num", meshInFilename;
    std::string rhoVecFile = NOT_DEFINED, saveModel = NOT_DEFINED;

    bool verbose = false, singularityRemoval = false, analytical = false, vectorOutput = false;
    bool dipoleSources = false, allOne = false, refineInterMesh = false, complex = false;
    bool refineH = false, refineP = false;

    OptionMap oMap;
    oMap.setDescription("Description. DCMod - Forward modelling of dc resistivity data on a given mesh\n");
    oMap.addLastArg(meshInFilename, "Mesh file");
    oMap.add(verbose            ,      "v" , "verbose"            , "Verbose mode.");
    oMap.add(allOne             ,      "1" , "allOne"             , "Set all conductivities to one.");
    oMap.add(singularityRemoval ,      "S" , "singularity-removal", "Use singularity removal.");
    oMap.add(analytical         ,      "A" , "analytical"         , "Analytical solution using a homogeneous half-space.");
    oMap.add(vectorOutput       ,      "V" , "VectorOutput"       , "Save potentials in several single files.");
    oMap.add(dipoleSources      ,      "D" , "DipolCurrentPattern" , "Calculate with dipol-current pattern instead of pol sources. Pattern will be determined automatically from given scheme file.");
    oMap.add(refineInterMesh    ,      "R" , "refineInterMesh"    , "Refine mesh for interpolation given with -m .");
    oMap.add(refineH            ,      "H" , "refineH"            , "Refine forward mesh spatially (h-refine).");
    oMap.add(refineP            ,      "P" , "refineP"            , "Refine forward mesh polynomially (p-refine).");
    oMap.add(attributemap       ,      "a:" , "attributeMap"      , "map file for attributes.");
    oMap.add(rhoVecFile         ,      "r:" , "rho"               , "set rho values for each cell");
    oMap.add(boundarymap        ,      "b:" , "boundaryMap"       , "map file for boundary conditions.");
    oMap.add(bypassmap          ,      "c:" , "bypassmap"         , "map file for bypass conditions.");
    oMap.add(meshToInterpolate  ,      "m:" , "meshtointerpolate" , "interpolate output to another mesh (e.g. for SR).");
    oMap.add(outputFileBody     ,      "o:" , "output"            , "basename for collectfile and potential files.");
    oMap.add(potentialsFileName ,      "p:" , "potentials"        , "save potentials to file.");
    oMap.add(schemeFile         ,      "s:" , "schemefile"        , "scheme file output will be a corresponding ohm-file.");
    oMap.add(primPotFileBody    ,      "x:" , "primaryFileBody"   , "file body for primary potential ");
    oMap.add(saveModel          ,      ":"  , "saveModel"         , "Save the used model into the given file.");
    oMap.parse(argc, argv);

    if (verbose) {
        std::cout << "Mesh = " << meshInFilename << std::endl;
        std::cout << "Singularity removal = " << singularityRemoval << std::endl;
        std::cout << "Attribute map = " << attributemap << std::endl;
        std::cout << "Boundary map = " << boundarymap << std::endl;
        std::cout << "Bypass map = " << bypassmap << std::endl;
    }

    //** Start declaration and initialisation;
    DataContainerERT * data = NULL;

    if (schemeFile != NOT_DEFINED) {
        data = new DataContainerERT();
        data->load(schemeFile);
        if (verbose){
            std::cout << schemeFile << ": electrodes: " << data->sensorCount()
                      << " data: " << data->size() << std::endl;
        }
    }
    std::string outstr("a b m n r");

MEMINFO
    Mesh mesh(meshInFilename);
    if (verbose) {
        std::cout << "Mesh: " << meshInFilename << " loaded." << std::endl;
        mesh.showInfos();
    }
MEMINFO

    //** Do on-the-fly h or p refinement
    if (refineH) {
        Mesh tmpMesh(mesh);
        mesh = tmpMesh.createH2();
        if (verbose) {
            std::cout << "On-the-fly mesh refinement (h): ";
            mesh.showInfos();
        }
    }
    if (refineP) {
        Mesh tmpMesh(mesh);
        mesh = tmpMesh.createP2();
        if (verbose) {
            std::cout << "On-the-fly mesh refinement (p): ";
            mesh.showInfos();
        }
    }

    //** resistivity mapping options
    if (allOne) { //** all conductivities const=1 S/m
        mesh.setCellAttributes(RVector(mesh.cellCount(), 1.0));
        if (verbose) std::cout << "set rhoa = 1.0" << std::endl;
    } else if (rhoVecFile != NOT_DEFINED){ //** rho vec dominates
        RVector rho(rhoVecFile);
        if (verbose) std::cout << "set rhoa: " << rhoVecFile << std::endl;
        mesh.setCellAttributes(rho);
    } else if (attributemap != NOT_DEFINED) { //** attribute map
        if (verbose) std::cout << "Performing resistivity mapping using attribute map: " << attributemap << std::endl;

        try {
            setComplexResistivities(mesh, loadCFloatMap(attributemap));
            if (verbose) std::cout << "Found complex attribute map." << std::endl;
            complex = true;
        } catch(...) {
            mesh.mapCellAttributes(loadFloatMap(attributemap));
        }
    }
    if (mesh.findCellByAttribute(0.0).size()){
        if (verbose) std::cout << "Performing auto resistivity mapping of " << mesh.findCellByAttribute(0.0).size() << " empty cells." << std::endl;
#ifdef GIMLI_USE_POSVECTOR // gimli 1.1
        RVector vals(mesh.cellAttributes());
        mesh.prolongateEmptyCellsValues(vals, -1.0);
        mesh.setCellAttributes(vals);
#else
        mesh.fillEmptyCells(mesh.findCellByAttribute(0.0), -1.0);
#endif
    }

    if (saveModel != NOT_DEFINED) mesh.cellAttributes().save(saveModel);

    if (boundarymap  != NOT_DEFINED) {
        if (verbose) std::cout << "Performing boundary mapping using boundary map: " << boundarymap << std::endl;
        mesh.mapBoundaryMarker(loadIntMap(boundarymap));
    }

    DCMultiElectrodeModelling * forward;
    if (singularityRemoval) {
        forward = new DCSRMultiElectrodeModelling(mesh, verbose);
        dynamic_cast< DCSRMultiElectrodeModelling * >(forward)->setPrimaryPotFileBody(primPotFileBody);
    } else { //** if not sing removal
        forward = new DCMultiElectrodeModelling(mesh, verbose);
    }
    forward->setDipoleCurrentPattern(dipoleSources);
    forward->setAnalytical(analytical);
    forward->setBypassMapFile(bypassmap);

    // set this after dipoleSources
    if (data) forward->setData(*data);

    RMatrix subPotentials;
    if (!complex){
        forward->collectSubPotentials(subPotentials);
    }

    if (singularityRemoval) {
        //** force retrieving G from primary potentials;
        outstr += " k rhoa";
    }

MEMINFO
    if (data && dipoleSources){
        if (complex) THROW_TO_IMPL
        forward->calculate(*data);
    } else { //** calculate pole-pole potentials;
        DataMap dMap;
        forward->calculate(dMap);
        dMap.save(outputFileBody + ".collect");
        if (data) {
            data->set("u", dMap.data(*data));
            if (complex) {
                setComplexData(*data, dMap.data(*data),
                               dMap.data(*data, false, true));
                outstr += " ip";
            }
        }
    }
MEMINFO

    if (data) { //** scheme file given by -s ;
        std::string outFile(outputFileBody + ".ohm");

        if (outputFileBody != "num"){
            outFile = (outputFileBody.substr(0, outputFileBody.rfind(".ohm")) + ".ohm");
        } else {
            if (schemeFile.rfind(".shm") != std::string::npos) {
                outFile = schemeFile.substr(0, schemeFile.rfind(".shm")) + ".ohm";
            }
        }

        data->set("i", RVector(data->size(), 1.0));
        data->set("r", data->get("u") / data->get("i"));
        data->save(outFile, outstr);
    }

    //** collect matrix for potentials;
    RMatrix potentials, subpotentials;

    if (meshToInterpolate != NOT_DEFINED) { //** instant interpolation;
        Mesh outMesh;
        if (refineInterMesh) {
             Mesh meshTmp(meshToInterpolate);
             outMesh = meshTmp.createH2();
        } else {
            outMesh.load(meshToInterpolate);
        }
        if (verbose){
            std::cout << "interpolate potential to " << std::endl;
            outMesh.showInfos();
        }

        interpolate(mesh, forward->solution(), outMesh.positions(), potentials, verbose, 0.0);

        if (mesh.dim() == 2){
            interpolate(mesh, subPotentials, outMesh.positions(), subpotentials, verbose, 0.0);
        }
    } else {
        potentials = forward->solution();
        subpotentials = subPotentials;
    }
    //** Output (sub)potentials;
    if (potentialsFileName != NOT_DEFINED) {
        if (verbose) {
            std::cout << "Save potential matrix (" << potentialsFileName << ".bmat) ."<< std::endl;
        }
        save(potentials, potentialsFileName);

        if (mesh.dim() == 2) {
            if (verbose){
                std::cout << "Save wavenumber potential matrix (" << potentialsFileName + "_s.bmat) ."
                          << subpotentials.rows() << " " << subpotentials.cols() << std::endl;
            }
            save(subpotentials, potentialsFileName + "_s");
        }

        if (vectorOutput) {
            if (verbose) {
                std::cout << "Save single potential files ("
                            << potentialsFileName + ".XXX.pot)" << std::endl;
            }
            for (size_t i = 0; i < potentials.rows(); i ++) {
                save(potentials[i], potentialsFileName + "." + str(i) + ".pot", Binary);
                if (verbose) std::cout << ".";
            }
            if (verbose) std::cout << std::endl;

            if (mesh.dim() == 2) {
                //** The s stands for single, that means the potentials for each singe wavecount k
                if (verbose) {
                    std::cout << "Save single wavenumber potential files ("
                            << potentialsFileName + ".XXX.s.pot)" << std::endl;
                }
                save(subpotentials, potentialsFileName + ".s");

                for (size_t i = 0; i < subpotentials.rows(); i ++) {
                    save(subpotentials[i], potentialsFileName + "." + str(i) + ".s.pot", Binary);
                    if (verbose) std::cout << ".";
                } // subpotentials
            if (verbose) std::cout << std::endl;
            } // dim=2
        } // vector output
        if (verbose) std::cout << std::endl;
    }

    delete forward;
    delete data;
    return EXIT_SUCCESS;
}
