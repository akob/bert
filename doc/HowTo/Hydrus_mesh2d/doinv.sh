nsel=36 # number of surface electrodes
ERR=1
infile=hydrus2d.ohm
outfile=out0.dat
datfile=syn.dat
# create dummy data file with only surface electrodes
echo $nsel > $outfile
head -n 2  $infile|tail -n 1 >> $outfile
head -n $[nsel + 2] $infile|tail -n $nsel | tac >> $outfile
echo 0 >> $outfile

# Noisify synthetic data with Gaussian Noise
dcedit -vSEN -e $ERR -u 0 -o $datfile $infile

# create cfg file for generating meshes
bertNew2DTopo $outfile > bert0.cfg
echo PARADEPTH=3 >> bert0.cfg
echo PARADX=0.2 >> bert0.cfg

# do only the meshes with the dummy file
bert bert0.cfg meshs
# create normal cfg file and do inversion
bertNew2DTopo $datfile > bert.cfg
echo LAMBDA=300 >> bert.cfg
echo ZWEIGHT=0.1 >> bert.cfg
echo BLOCKYMODEL=1 >> bert.cfg
#cp mesh.bms mesh/  # use Hydrus mesh for inversion
bert bert.cfg pot calc
