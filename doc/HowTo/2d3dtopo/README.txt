Howto do 3d visualization of 2d inverted profiles and 3d inversion of them

Since true 3d measurements (grid of electrodes) are often prohibitive due to limited electrodes, usually pseudo-3d, i.e. 2d profiles - not necessarily parallel and perpendicular, are measured. 

Task: Generate 2d and 3d data files from 3d digital elevation model DEM and visualize results

Problem: 2d profiles are local (tape-measure) and DEM global

Solution: reading pro-file, tape-unrolling and vtk file manipulation

Files:
    topo.xyz -- digital elevation model DEM, total stations for georeferencing

    p1|p2|p3|p4|p5|p6|p7.dat -- 2d data files with local coordinates without topography

    alldata.pro -- Profile file. Provide the absolute locations of all profiles by a polygon of 2 or more points.
    (used e.g., by DC3dInvRes) looking like:
        filename_of_2dfile x1 y1 x2 y2 ...
    
    mk3ddata.py -- python script to interpolate the topography from the DEM into the 2d files with unrolling the tape measure
    mk3ddataPG.py -- the same as above but with pygimli interpolator

    transformvtk.py ---

    doall.sh -- shell script that do all

-------- End




Whereas the 3d file already has the correct 3d coordinates, we must back-transform the resulting vtk file. Assume we have the results named the same way as the 2d data files but with the extension vtk. We read in the vtk file and the map file previously created:

    t,x,y,z,x2d = P.loadtxt( 'positions/' + datafile.replace('.dat','.txyz') ).T

    vtkfile = datafile.replace('.dat','.vtk')
    mesh = g.Mesh( vtkfile )

    xm = g.x( mesh.positions() ) # 2d x
    zn = g.z( mesh.positions() ) # z

Then we interpolate from the 2d coordinates to the 3d coordinates. Instead of numpy.interp we need also to extrapolate since the mesh usually goes beyond the electrodes. For this reason we write a simle interpolation-extrapolation function:

def myextrap(x, xp, yp):
    """ numpy.interp function with linear extrapolation """
    y = P.interp(x, xp, yp)
    y = P.where(x < xp[0], yp[0]+(x-xp[0])*(yp[0]-yp[1])/(xp[0]-xp[1]), y)
    y = P.where(x > xp[-1], yp[-1]+(x-xp[-1])*(yp[-1]-yp[-2])/(xp[-1]-xp[-2]), y)
    return y

and use this function for determining x and y positions of the mesh nodes

    xn = myextrap( xm, x2d, x )
    yn = myextrap( xm, x2d, y )

Finally we set the positions of the mesh nodes and export the vtk.

    for i in range( mesh.nodeCount() ):
        mesh.node(i).setPos( g.RVector3( xn[i], yn[i], zn[i] ) )

    mesh.exportVTK( '3dvtk/' + vtkfile )

Now all vtk files in the folder 3dvtk can be loaded into paraview to form a fence diagram. Additionally the result from the 3d inversion can be loaded and compared to the 2d inversions.

If the topography does not show a large curvature, alternatively the 2d inversions could be done with the original (tape measure) files and the back-transformation will add the topography:

    xn = myextrap( xm, t, x )
    yn = myextrap( xm, t, y )
    zn = zn + myextrap( tn, t, z )
    for i in range( mesh.nodeCount() ):
        mesh.node(i).setPos(  g.RVector3( xn[i], yn[i], zn[i] ) )
        
The resulting vtk files can be viewed together in ParaView and their visualization is very easy to control by grouping the files.
For even more convenience, the meshes can be joined into a single vtk.