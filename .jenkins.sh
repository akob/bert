# This script is for continuous integration using Jenkins (http://jenkins-ci.org/)
# It is called from the parent directory, i.e. bash -xe trunk/.jenkins.sh

echo "Starting automatic build #$BUILD_NUMBER on" `date`
start=$(date +"%s")

# Show last change to repo in build log
echo `git --git-dir trunk/.git log -1 --pretty="Last change by %cn (%h): %B"`

# link python to python3
ln -sf /usr/bin/python3 python
export PATH=`pwd`:$PATH

# Show system information
lsb_release -d
uname -a
gcc --version
cmake --version
python --version
python -c "import numpy; print(numpy.__version__)"

################
#  Main build  #
################

# Location to GIMLI_PATH is set by jenkins

# Set paths
export PYTHONPATH=$GIMLI_PATH/trunk/python:$PYTHONPATH
export BERTTHREADS=4

# Main build
rm -rf build
mkdir -p build
cd build

cmake ../trunk \
    -DGIMLI_LIBRARIES=$GIMLI_PATH/build/lib/libgimli.so \
    -DGIMLI_INCLUDE_DIR=$GIMLI_PATH/trunk/src \
    -DPYVERSION=3 \
    -DPYTHON_EXECUTABLE=/usr/bin/python3 \
    -DPYTHON_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython3.5m.so

make bert1 dcinv dcmod dcedit

export PYTHONPATH=`pwd`/../trunk/python:$PYTHONPATH
python -c "import pybert; print(pybert.__version__)"

# Run gallery example
export PATH=`pwd`/bin:$PATH
cp -rf ../trunk/examples/inversion/2dflat/gallery .
cd gallery
MPLBACKEND="Agg" bert gallery.cfg all || true
if [[ ! -f resistivity.vector ]] ; then
    echo "Something went wrong in gallery example."
    exit 1
fi

end=$(date +"%s")
echo "Ending automatic build #$BUILD_NUMBER".
diff=$(($end-$start))
echo "$(($diff / 60)) minutes and $(($diff % 60)) seconds elapsed."
