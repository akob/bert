#!/usr/bin/env bash

echo "-1 -1 #surface" > neumannBnd.map
echo "-2 -1 #earth"   >> neumannBnd.map

echo "1 1 #background"      > srrho.map
echo "2 1.001 # SR-testcube">> srrho.map

echo "1 1 #background"      > rho.map
echo "2 1 # SR-testcube"    >> rho.map

verbose=''
QUALITY3D=1.2
dimension=3;

# Chosse electrode shape
# electrodes="cem";
# electrodes="free"; withData='-s test.shm'
# electrodes="node";

# Toggle quadratic shape functions
# p2mesh='-p'
# p2mesh=''

# Toggle singularity removal
# for sing. remov. you need a heterogeneous resistivity distribution
# SRTEST='';   RHOMAP=rho.map
# SRTEST='-S'; RHOMAP=srrho.map

# Use Reference electrode
# withReference=1

# Use calibration point for homogeneous boundary conditions
# withCalibration=1

# Use homogeneous boundary conditions, overwrite default boundary conditions
# neumann='-b neumannBnd.map'
# neumann=''

# Use datafile based solution collection
# withData='-s test.shm'
# withData=''

testAll(){
    withReference=0; withCalibration=0; neumann=''; withData=''; DIPOLE=''
    dimension=2;
        electrodes="node"
            analytical='-A'; p2mesh='';   SRTEST=''; source ./dcTest.sh 0.0203
            analytical='-A'; p2mesh='-p'; SRTEST=''; source ./dcTest.sh 0.021
            analytical='';   p2mesh='';   SRTEST=''; source ./dcTest.sh 2.5
            analytical='';   p2mesh='-p'; SRTEST=''; source ./dcTest.sh 0.21
            analytical='-A'; p2mesh='';   SRTEST='-S'; source ./dcTest.sh 0.0203
            analytical='-A'; p2mesh='-p'; SRTEST='-S'; source ./dcTest.sh 0.021
            analytical='';   p2mesh='';   SRTEST='-S'; source ./dcTest.sh 0.021
            analytical='';   p2mesh='-p'; SRTEST='-S'; source ./dcTest.sh 0.021
        withData='-s test.shm';
            DIPOLE='-D'
            analytical='-A'; p2mesh=''; SRTEST=''; source ./dcTest.sh 0.024
        electrodes="free"; DIPOLE=''
            analytical='-A'; p2mesh='';   SRTEST='';   source ./dcTest.sh 3.0
            analytical='-A'; p2mesh='-p'; SRTEST='';   source ./dcTest.sh 0.7
            analytical='';   p2mesh='';   SRTEST='';   source ./dcTest.sh 2.7
            analytical='';   p2mesh='-p'; SRTEST='';   source ./dcTest.sh 1.26
            analytical='-A'; p2mesh='';   SRTEST='-S'; source ./dcTest.sh 2.4
            analytical='-A'; p2mesh='-p'; SRTEST='-S'; source ./dcTest.sh 0.7
            analytical='';   p2mesh='';   SRTEST='-S'; source ./dcTest.sh 2.4
            analytical='';   p2mesh='-p'; SRTEST='-S'; source ./dcTest.sh 0.7
            DIPOLE='-D'
            analytical='-A'; p2mesh='';   SRTEST=''; source ./dcTest.sh 2.34

    dimension=3;
        electrodes="node"; withData=''; DIPOLE=''
            analytical='-A'; p2mesh='';   SRTEST=''; source ./dcTest.sh 1e-12
            analytical='-A'; p2mesh='-p'; SRTEST=''; source ./dcTest.sh 1e-12
            analytical='';   p2mesh='';   SRTEST=''; source ./dcTest.sh 5.0
            analytical='';   p2mesh='-p'; SRTEST=''; source ./dcTest.sh 0.4
         electrodes="free"; withData='-s test.shm'
            analytical='-A'; p2mesh='';   SRTEST=''; source ./dcTest.sh 6
            analytical='-A'; p2mesh='-p'; SRTEST=''; source ./dcTest.sh 1.4
            analytical='';   p2mesh='';   SRTEST=''; source ./dcTest.sh 25
            analytical='';   p2mesh='-p'; SRTEST=''; source ./dcTest.sh 2.3
        electrodes="cem"; withData=''
            analytical=''; p2mesh=''; SRTEST=''; source ./dcTest.sh 3.9

}

withReference=0; withCalibration=0; neumann=''; withData=''
dimension=2; withData='-s test.shm'
#electrodes="node"; analytical='-A'; p2mesh=''; SRTEST=''; DIPOLE=''; source ./dcTest.sh 0.024
#electrodes="node"; analytical='-A'; p2mesh=''; SRTEST=''; DIPOLE='-D'; source ./dcTest.sh 0.024
#electrodes="free"; analytical='-A'; p2mesh=''; SRTEST=''; DIPOLE='-D'; source ./dcTest.sh 2.33

# dimension=2, analytical=,
#electrodes="node"; DIPOLE=''; analytical=''; p2mesh='-p'; SRTEST=''; source ./dcTest.sh 0.21

#electrodes="free"; DIPOLE=''; analytical=''; p2mesh=''; SRTEST=''; source ./dcTest.sh 2.7

#EXIT()
# dimension=3; electrodes="node"; analytical='-A'; p2mesh=''; SRTEST=''; source ./dcTest.sh
# dimension=3; electrodes="free"; analytical='-A'; p2mesh=''; SRTEST=''; source ./dcTest.sh

#dimension=3; electrodes="node"; analytical=''; p2mesh=''; SRTEST=''; source ./dcTest.sh 5.0
#exit

testAll
