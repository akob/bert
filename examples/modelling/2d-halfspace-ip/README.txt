Derived from 2d-halfspace-two-layer and added a rectangular block

24 Electrodes Wenner-alpha array on two layered earth.
Top layer 10 Ohmm with 1m thickness and 100 Ohmm Background.
Rectangular block with real and complex resistivity.

The shell script way: calc.sh

Files:
    README.txt -- this file

    calc.sh -- the main calculation script

    wa24.shm -- configuration for Wenner-Alpha measurement with 24 electrodes
